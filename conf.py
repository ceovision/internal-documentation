# -*- coding: utf-8 -*-

from __future__ import division, print_function, unicode_literals

from datetime import datetime

from recommonmark.parser import CommonMarkParser

extensions = []
templates_path = ['/home/docs/checkouts/readthedocs.org/readthedocs/templates/sphinx', 'templates', '_templates', '.templates']
source_suffix = ['.rst', '.md']
source_parsers = {
            '.md': CommonMarkParser,
        }
master_doc = 'index'
project = u'internal-documentation'
copyright = str(datetime.now().year)
version = 'latest'
release = 'latest'
pygments_style = 'sphinx'
htmlhelp_basename = 'internal-documentation'
html_theme = 'sphinx_rtd_theme'
file_insertion_enabled = False

language = 'fr'
gettext_compact = False

