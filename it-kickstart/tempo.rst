********************************************
PROC - Time Log (Tempo)
********************************************

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
  JLE          03/01/2023       V1.0
========= ==================== =======

Utiliser tempo
=====================

Tous les membres de l'équipe IT de CEO-Vision enregistrent leur temps passé sur chaque tâche. Pour ce faire, nous utilisons le Plugin 'Tempo' de Jira, ce qui nous permet de lier les tickets aux enregistrements.

Enregistrer son temps
-----------------------------------

L'accès à Tempo se fait depuis les applications de Jira : 

.. image:: img/tempo-access.png

Voici comment se compose l'interface de Tempo : 

.. image:: img/tempo-view.png

- 1. Suivi du temps passé par tâche
- 2. Utilisez ce bouton pour enregistrer un temps sur une tâche
- 3. Il est possible de changer de vue et de passer en mode calendrier, à la semaine par exemple
- 4. Ce bouton sert à envoyer ses timelog, il doit être utilisé à la fin du mois
- 5. En cliquant sur chaque case il est possible d'y renseigner un temps

Une fois que toutes les tâches ont été loggués sur un mois, le système propose de soumettre la feuille de temps pour approbation : 

.. image:: img/tempo-submit.png

.. WARNING::
   Les feuilles de temps doivent être soumises au plus tard le 10 de chaque mois pour la période précédente

Jira spécifiques pour enregistrer un temps
--------------------------------------------

Parfois, certaines tâches ne concernent pas directement un Jira. Pour ces cas, il est possible d'enregistrer son temps sur des tickets spécifiques : 

- **CEOVIS-535** : Support client, interventions en production, ticketing, réponses e-mail...

- **CEOVIS-585** Tâches techniques interne (Intervention sur les serveurs CEO-Vision, hébergement etc)

- **CEOVIS-554** Tâches non techniques interne (RH/Mgt/Formation/Administratif). Attention : Certaines tâches techniques de management comme la validation de PR ou la review de code peuvent entrer dans cette catégorie

- **GFQAS-67** : Tâches relatives à la partie qualité n’entrant pas dans le cadre d’un Jira qualité (réunions, ..)

- **PRJ-XXX** Tâches X.X.X (Exemple CEOVIS-548 Taches 4.0.X) : Tâches relatives à la production d’une release (Build, réunions GF4, MER/MEP sur environnements internes...)

- **CEOVIS-210** : Autre, ne pouvant être classé (**à utiliser le moins possible**)


Synchronisation avec le calendrier
====================================

Cette procédure détaille les étapes de synchronisation du calendrier BlueMind avec l’application Tempo de Jira.

**Attention** : Cette procédure nécessite d’envoyer les données de notre calendrier à google calendar ce qui n’est pas souhaité mais malheureusement nécessaire pour l’intégration.

Renseigner l’agenda
----------------------

La première chose à faire est de renseigner correctement son agenda avec ses évènements de la journée de la manière suivante : NUM-XXX DESCRIPTION

- **NUM-XXX** Corespond au numéro de Jira

- **DESCRIPTION** Correspond à la description de la tâche

.. image:: img/tempo-cal.png

Export de l’agenda au format iCal
----------------------------------

Sur l’interface Bluemind (https ://bluemind.ceo-vision.com), aller dans son agendaet dans la partie 'Calendriers’ ne garder actif que son calendrier : 

.. image:: img/tempo-cal-filter.png

Puis dans le menu en haut à droite, exporter le calendrier : 

.. image:: img/tempo-export.png

Import de l’agenda dans Google Calendar
------------------------------------------

Sur Google Calendar dans « Autres agendas » cliquer sur importer : 

.. image:: img/tempo-import1.png

Séléctionner le fichier exporté depuis Bluemind et cliquer sur « Importer »

.. image:: img/tempo-import2.png

Synchronisation avec Tempo
------------------------------

Dans l’application Tempo, cliquer sur le bouton « Apps » dans le menu de gauche

.. image:: img/tempo-apps.png

Choisir « Google Calendar »

.. image:: img/tempo-gcal.png

**Important** : Choisir « Basic calendar connection », la connexion améliorée ne fonctionne pas bien.

.. image:: img/tempo-conf.png


Renseigner Tempo
-------------------

Ouvrir la vue « Calendar » en haut à droite

.. image:: img/tempo-cal-view.png

Si le calendrier est correctement renseigné, il suffit de valider les différents évènements

.. image:: img/tempo-log-direct.png

Sinon on peut aussi cliquer dessus pour faire des modifications avec les données pré remplies

.. image:: img/tempo-log-indirect.png
