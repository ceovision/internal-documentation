********************************************
PROC - Git : Gestion des branches
********************************************

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
  JLE          30/12/2022       V1.1
  JLE          21/08/2020       V1.0
========= ==================== =======

Documentation Développeur
==============================================

Démarrer un nouveau développement localement
----------------------------------------------

La première chose à faire pour démarrer un nouveau développement est de savoir dans quelle version ce développement est prévu. Cette information se trouve dans le ticket Jira associé.

Une fois cette information disponible, je checkout la branche concernée, portant le nom **dev/x.x.x**. 

Par exemple ici je souhaite démarrer une développement pour la version 4.1.0 : 

.. image:: img/git-checkout.png

**Attention** à bien être à jour sur la branche de développement (Faire un pull).

Créer une branche correspondant à son développement : type/NUM_JIRA

Type peut prendre une des valeurs suivantes : 

* **bug** => Il s'agit d'un développement pour corriger un bug de l'application
* **feat** => Il s'agit d'un développement pour améliorer ou créer une fonctionnalité

Dans mon exemple, je vais créer un correctif de bug pour la version 4.1.0 avec le Numéro de Jira : GOFAST-9999

Ma branche se nommera donc bug/GOFAST-9999

.. image:: img/git-new-branch.png

A partir d'ici je peux commencer à développer ma fonctionnalitée.

Commit mes développements
---------------------------

Il y a quelques règles de base à suivre pour faire de bons commits :

* Un commit doit dans la mesure du possible constituer une modification **simple** et facilement lisible avec un message de commit clair.
* Un commit doit pouvoir être revert facilement en cas de besoin, c'est un bon moyen de savoir si il s'agit ou non d'un bon commit
* Un message de commit doit être sous la forme "NUM_JIRA Message de commit"
* Les modifications de mise en forme d'un fichier doivent se faire dans des branches séparés. Ne **jamais** reformater tout ou grande partie d'un fichier dans un de vos développements.
* Un commit ne dois **jamais** embarquer de modifications d'un autre développeur si vous travaillez sur un serveur partagé

Pour ce dernier point en particulier, attention à soigneusement étudier chaque modifications apportés et à séléctionner uniquement les votres. Les autres modifications peuvent être **discard**.

.. image:: img/git-diff.png

Il est capital d'être **à l'aise** avec les modifications que vous allez envoyer, d'en comprendre conséquences et vérifier chaque étape.

Egalement, n'hésitez pas à faire plusieurs commits si vous avez modifié des éléments séparés.

.. NOTE::
   Certaines politiques de développement imposent un nombre maximal de caractères ou d'écraser tous
   les commits d'une branch pour n'en faire qu'un seul. Nous n'appliquons cette politique en aucun cas par soucis
   de tracage de chaque changement.

Vous pouvez savoir qu'un développement est correctement poussé sur le serveur distant quand les deux icones "PC" et "GitLab" sont présentes sur l'étiquette de la branche.

.. image:: img/git-commit.png

Faire une Merge Request
---------------------------

Une fois votre développement terminé **et testé** sur une plateforme de développement, il est temps de demander une intégration à la branche de développement ciblée.

Pour ce faire il existe deux méthodes, soit depuis GitKraken dans la partie "Remote" en effectuant un glissé déposer de votre branche vers la branche cible : 

.. image:: img/git-drag-drop.png

Vous pouvez alors remplir les champs comme ceci et valider votre Merge Request : 

.. image:: img/git-mr1.png

Ou soit directement depuis GitLab : 

.. image:: img/git-mr2.png

Une fois votre Merge Request enregistrée, ce n'est pas terminé ! Il faut absolument penser à aller sur la page de votre Merge Request pour vérifier son contenu.

Vous pouvez y accéder directement depuis GitKraken : 

.. image:: img/git-mr-go.png

Tout particulièrement dans la partie "Changes", vous ne devez voir que vos changements.

.. image:: img/git-mr-check.png

.. WARNING::
   Une merge request est la dernière étape avant qu'un développement soit accepté pour être intégrée à la version qui sera mise en recette.

Documentation Maintainer
==============================================

Créer une nouvelle version
-----------------------------

* La branche doit être crée depuis **master**
  
* La branche porte le nom dev/x.x.x

* Créer les scripts d'update directement sur la branch de dev. (OBSOLETE quand GoFAST NG)

  * **Attention** a bien repartir du dernier script d'update et à vérifier si il n'y a pas de TODO à l'interieur

  * **Attention** aux branches de développements en parallèle qui pourraient intégrer d'autres modifications dans les parties COMMON des scripts

Créer un hotfix
------------------

* La branche doit être crée depuis **master**
  
* La branche porte le nom hotfix/x.x.x

* Créer les scripts d'update directement sur la branch de dev. (OBSOLETE quand GoFAST NG)

  * **Attention** a bien repartir du dernier script d'update et à vérifier si il n'y a pas de TODO à l'interieur

  * **Attention** aux branches de développements en parallèle qui pourraient intégrer d'autres modifications dans les parties COMMON des scripts

Préparer une Release Candidate
------------------------------------

* Vérifier qu'il ne reste pas des branches ouvertes sur cette version en utilisant l'écran de merge request de Gitlab (vérifier également sur master en cas d'erreur de développeurs)

* Exporter les traductions (.po) et build les scss pour tout mettre à jour

* Modifier en masse les fichiers .install avec la nouvelle version

* Merge la branche de développement dans master

  * **Attention** Les messages de commit sont conventionnés au niveau du message : "RELEASE x.x.x RCY Merge dev/x.x.x into 'master'"

.. image:: img/rc-commit-message.png

* Tag le commit qui va être déployé

.. WARNING::
   Cette étape est **primordiale** pour les opérations de suivi et de production.
   Merci de respécter cette procédure scrupuleusement.

.. image:: img/rc-commit-tag.png

Fermer une version en validant une Release Candidate
------------------------------------------------------

* Tag sur le dernier commit RC

.. image:: img/rel-commit-tag.png

* Suppression de la branch de développement