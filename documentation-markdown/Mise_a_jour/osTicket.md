# Mise à jour osTicket APP2
---

Impact possible: Timeout sur les sites web hébergés sur APP2 (teampass,mautic,helpdesk) lors du snapshort de la VM 

## Étape 1 - Backup

Vérification des versions nécessaires (PHP - 7.4 | MySQL 5.7)

```bash
php -v
mysql -u root -p -v
```

Création du backup (**faire un snapshort et vérifier veeam**)

```bash
cd /var/www/html/
tar -cvf osticket_backup.tar.gz osticket
cp ./osticket/include/ost-config.php .
mysqldump -u root -p --databases osticket --events > osticket_backup.sql
ls
```

Passer le système de ticket en **mode offline**.

## Étape 2 - Envoyer les fichiers de MAJ

Envoyer le **contenu du dossier upload** dans **/var/www/osticket** et les **langues** dans **/var/www/html/osticket/include/i18n/**

```bash
# Via CLI ou un logiciel type WinSCP
scp fr.phar root@app2-preprod.ceo-vision.com:/var/www/html/osticket/include/i18n/
scp en_GB.phar root@app2-preprod.ceo-vision.com:/var/www/html/osticket/include/i18n/
```

Re-placer le fichier de configuration
```bash
cp ./ost-config.php ./osticket/include/
```


## Étape 3 - Lancer la MAJ via l'UI WEB

## Étape 4 - Supprimer le dossier setup

```bash
cd /var/www/html/osticket/
rm -rf setup/
```

## Étape 5 - Tester osTicket après la mise à jour

Réactiver osTicket, passage en **mode online**

- Envoie d’un mail ([osticket_testmail](https://helpdesk-.ceo-vision.com/scp/emailtest.php))
- Création d’un ticket par mail (envoyer un email à **helpdesk@ceo-vision.com**)
- Création d’un ticket par l’interface 
- Upload d’image (en répondant au ticket créé par email)
- Réponse à un ticket (ouverture/réponse/fermeture etc…)