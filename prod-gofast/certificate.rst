**********************************************
Procédure : Mise à jour des certificats GOFAST
**********************************************

Cette procédure décrit comment effectuer la mise à jour des certificats GOFAST.

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
  EPA          04/10/2022       V1.1
  BCR          12/09/2022       V1.0
========= ==================== =======

Pré-requis :
============

- Les certificats (wildcard ou non) au format ...
- Un accès aux machines **MAIN** & **COMM**

Installation :
==============

Machine MAIN
------------

::
    
    # Définition des variables nécessaires
    echo "Entrer votre URL GoFAST MAIN :" && read gofast_main
    echo "Entrer votre URL GoFAST MOBILE :" && read gofast_mobile

    # Pour commencer, on stop les services web.
    systemctl stop httpd
    systemctl stop nginx

    # Puis, on fait un backup des certificats existants :
    mkdir /opt/backup_certs
    \cp -Rf /etc/pki/tls/certs/localhost.crt /opt/backup_certs/localhost.crt
    \cp -Rf /etc/pki/tls/private/localhost.key /opt/backup_certs/localhost.key

    # Ensuite, il faut lancer le scrip acme.sh (situé dans /opt) :
    ./acme.sh --renew -d ${gofast_main} --insecure

    # Enfin, on copie les certificats dans les répertoires pour les remplacer :
    \cp -Rf /root/.acme.sh/${gofast_main}/${gofast_main}.cer  /etc/pki/tls/certs/localhost.crt
    \cp -Rf /root/.acme.sh/${gofast_main}/${gofast_main}.key /etc/pki/tls/private/localhost.key

    # Toujours sur la machine **MAIN**, il faut mettre à jour les certificats mobile (si le certificat n'est pas wildcard).
    # Pour ce faire, il faut de nouveau lancer le scrip acme.sh.

    \cp -Rf /etc/pki/tls/certs/localhost_mobile.crt  /opt/backup_certs/localhost_mobile.crt
    \cp -Rf /etc/pki/tls/private/localhost_mobile.key /opt/backup_certs/localhost_mobile.key
    ./acme.sh --renew -d ${gofast_mobile} --insecure
    \cp -Rf /root/.acme.sh/${gofast_mobile}/${gofast_mobile}.cer /etc/pki/tls/certs/localhost_mobile.crt
    \cp -Rf /root/.acme.sh/${gofast_mobile}/${gofast_mobile}.key /etc/pki/tls/private/localhost_mobile.key

    # Pour finir, on redémarre les services web de la machine.
    systemctl start httpd
    systemctl start nginx

Machine COMM
------------

::

    # Définition de la variable nécessaire
    echo "Entrer votre URL GoFAST COMM :" && read gofast_comm
    
    # On commence par stopper les services nginx et coturn, et on backup les anciens certificats :
    systemctl stop nginx
    systemctl stop coturn

    # création des backups des anciens certificats
    mkdir /opt/backup_certs
    \cp -Rf /etc/pki/tls/certs/gofast.crt /opt/backup_certs/gofast.crt
    \cp -Rf /etc/pki/tls/private/gofast.key /opt/backup_certs/gofast.key

    # Ensuite, on lance le script acme.sh (situé dans /opt), et on copie les certificats dans le bon répertoire.
    ./acme.sh --renew -d ${gofast_comm} --insecure
    \cp -Rf /root/.acme.sh/${gofast_comm}/${gofast_comm}.cer  /etc/pki/tls/certs/gofast.crt
    \cp -Rf /root/.acme.sh/${gofast_comm}/${gofast_comm}.key /etc/pki/tls/private/gofast.key
    
    # Enfin, on redémarre les services :
    systemctl start nginx
    systemctl start coturn

Test a effectuer pour valider le déploiement
============================================

- Connexion automatique du chat Element
- Co-édition d'un document (sauvegarde automatique et cadenas)
- Visio-conférence en lancant 3 onglets avec la caméra
- à compléter ...

Troubleshooting
===============

Cette partie décrit les éventuelles problèmes qui peuvent être rencontrés.

Problème connexion non automatique au chat Element (COMM)
---------------------------------------------------------

Sur la machine **COMM**, il faut exécuter les commandes suivante :
::
    cd /opt/gofast-comm
    echo "Enter GoFAST MAIN URL (ex: gofast.ceo-vision.com):"
    read url_main
    java InstallCert $url_main
    cp jssecacerts /etc/pki/ca-trust/extracted/java/cacerts
    systemctl restart ma1sd


.. note::
    ma1sd - service qui gère la connexion au chat Element

Problème Cadenas OnlyOffice et non sauvegarde du document (MAIN)
----------------------------------------------------------------

Commandes à exécuter sur la plateforme MAIN (en ssh) pour corriger le problème de cadenas qui reste fermé à la sortie d'une coédition OnlyOffice (attention ce problème provoque que toute édition sur le document ne sera pas sauvegardé).
::
    cd /opt
    echo "Enter GoFAST COMM URL (ex: gofast-comm.ceo-vision.com):"
    read url_comm
    java InstallCert $url_comm
    cp jssecacerts /etc/pki/ca-trust/extracted/java/cacerts
    echo "Warning la GED va redémarrer"
    sleep 2
    systemctl restart tomcat@alfresco

Problème Certificats Visio (COMM)
---------------------------------
.. warning::
    La GED va redémarrer suite à ces commandes, faites en sorte de vous assurer que personne ne travaille dessus.
Commandes à exécuter sur la plateforme COMM (en ssh) pour corriger le problème de visio-conférence à trois ou + impossible. (avec flux vidéo).

.. note::
    secret_key = mot de passe dans /etc/jitsi/jicofo/config, à récupérer avant lancement.


Commandes :
::
    server_name=`grep -oP "(?<=domain: \').+?(?=\')"
    /etc/ma1sd/ma1sd.yaml`
    read secret_key
    rm -Rf /var/lib/prosody/*
    cd /opt/gofast-comm/update
    systemctl stop crond
    prosodyctl stop
    systemctl stop jibri
    systemctl stop jitsi-videobridge
    systemctl stop jicofo
    #Generate certificates
    prosodyctl cert generate auth.${server_name}


.. note::
    Entrez les informations suivantes pour la commande "prosodyctl cert generate auth.${server_name}" :
    - 4096
    - FR
    - COMM
    - GOFAST
    - XMPP
    - auth.${server_name}
    - xmpp@auth.${server_name}


Et pour terminer :
::
    mv /var/lib/prosody/auth.$server_name.crt /var/lib/prosody/auth.crt
    mv /var/lib/prosody/auth.$server_name.key /var/lib/prosody/auth.key
    mv /var/lib/prosody/auth.$server_name.cnf /var/lib/prosody/auth.cnf
    ln -sf /var/lib/prosody/auth.crt /etc/pki/catrust/source/anchors/auth.crt
    update-ca-trust extract -f
    prosodyctl start
    prosodyctl register videobridge auth.$server_name $secret_key
    prosodyctl register jibri auth.$server_name $secret_key
    prosodyctl register recorder recorder.$server_name $secret_key
    prosodyctl register focus auth.$server_name $secret_key
    prosodyctl mod_roster_command subscribe focus.$server_name
    focus@auth.$server_name
    systemctl start jibri
    systemctl start jitsi-videobridge
    sleep 3
    systemctl start jicofo
    systemctl start crond

