**************************************************
PROC - Restauration d'un serveur avec Veeam
**************************************************

Cette procédure décrit les étapes pour effectuer une restauration à l'aide de Veeam Backup & Replication.
Celle-ci peut servir à :
- Restaurer une machine virtuelle après un incident.
- Cloner/déplacer une machine virtuelle.

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
BCR        22/09/2022           V1.0
========= ==================== =======

Pré-requis 
============

- Un accès admin à l'ESX où vous souhaitez restaurer la machine.
- Une adresse IP et une adresse MAC pour pouvoir joindre le serveur VEEAM depuis la machine virtuelle qui sera créé sur l'ESX.
- Le LiveCD de veeam (disponible dans les datastores).

Procédure
==========

Dans l'exemple ci-dessous, la **VM PRD-CEOV-APP2** va être restaurée afin de créer un **serveur de préproduction.**

Création d'une VM ISO de la source
-----------------------------------

Sur l'ESX où vous souhaitez effecteur la restauration, il faut instancier une nouvelle machine.
La nouvelle machine **doit** posséder la même configuration pour le **stockage**, le CPU et la RAM peuvent différer.

.. image:: img/config_vm.png

Dans la partie **Lecteur de CD/DVD 1**, il faut ajouter le LiveCD disponible dans le datastore.

.. image:: img/liveCD_veeam.png


Démarrage du live LiveCD
-------------------------

Une fois la nouvelle VM configurée, vous pouvez la démarrer. 
Le disque veam doit se lancer et vous devez tomber sur une page vous indiquant votre configuration réseau.

.. image:: img/veeam_netconfig.png

Choisissez **Continue**, puis accepté les termes de veeam.

.. note::
    espace - pour valider une case.
    tab - pour se déplacer dans le menu

Sur l'écran suivant, il faut configurer le réseau : **Configure network**.

.. image:: img/veeam_netconfig_menu.png

Puis remplir la partie IPv4.

.. image:: img/veeam_ipconfig.png

Vous pouvez également désactiver/activer l'interface pour être sûr que la configuration soit prise en compte, puis faire **Ok**.

Restauration de la VM
----------------------

Une fois la configuration réseau effectuée, retournez sur le menu principal du CD.
Choisissez **Restore volumes**:

.. image:: img/veeam_restVolume.png

Ensuite, choisir la restauration à partir d'un serveur VEEAM.

.. image:: img/veeam_restDest.png

Et renseigner les Informations de connexion au serveur VEEAM *(attention au clavier qui est en qwerty)*.

.. image:: img/veeam_login.png

Une fois connecté, la liste des backups s'affichent, il faut choisir le backup qui correspond à la machine et à la date de restauration voulue.
Dans ce cas, on choisira le dernier backup de APP2.

.. image:: img/veeam_chooseBck.png

Pour finir, cliquez sur chaque disque et choisissez **Restore from ...**. 

.. image:: img/veeam_restoreFrom.png

.. image:: img/veeam_restoreFromSelect.png

L'objectif est d'obtenir la même interface à droite qu'à gauche, et faites **S** pour commencer la restauration.

Une fois l'opération terminée, vous pouvez redémarrer, la nouvelle machine doit démarrer à son état restauré.

.. image:: img/veeam_finish.png

.. image:: img/veeamreboot.png

.. image:: img/veeamVMStart.png
    
Troubleshooting
===============

