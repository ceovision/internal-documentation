**************************************************
PROC - Création d'un dossier dans alfresco & drupal
**************************************************

Exemple avec le ticket : https://helpdesk.ceo-vision.com/scp/tickets.php?id=7396

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
BCR        11/10/2022           V1.0
========= ==================== =======

Contexte
============

Les dossiers extranet suivant créés par un utilisateur n'apparaissent pas dans GoFAST.

- _scd_accueil_bib  
- _scd_accueil_bib2

Après avoir donné accès au compte *adm*, on remarque que les espaces font partie de **Support**.
Ceux-ci **ne sont pas présent** dans Alfresco.

Procédure
==============

Étape 1 - Créer le dossier
---------------------------

Dans Alfresco, se positionner à l'endroit où se situent le dossier.
Ici, cela donne : Entrepôt> Sites>_Extranet>\_Support .
Puis, il faut créer un dossier. (*Renseigner uniquement le "Nom"*).

.. images:: img/20221011124720.png

Ensuite, sur le dossier, il faut aller dans **plus** et **gérer les aspects**.

.. image:: img/20221011122359.png

Verifier que **undefined (gofast:nodeproperties)** est présent.

.. image:: img/20221011114541.png

Puis **Éditer les propriétes**, **Toutes les propriéts**.
Remplir les parties :
- node id: ID du noeud
- bundle : extranet *(en minuscule) ( autres valeurs: group organisation extranet public)*
- cocher :  **is group** & **prevent deletion**

.. image:: img/20221011114656.png

De retour sur le dossier, **plus** ->  **gerer les droits d'accès**.
Il faut désactiver l'héritage et ajouter les utilisateurs du noeud. (format : _#NODEID)

- _#577194
- _#577194_ADMIN
- _#577194_STANDARD

.. image:: img/20221011114939.png

Puis attribuer les rôles suivants :

- _#577194 - Lecteur
- _#577194_ADMIN - Coordinateur
- _#577194_STANDARD - roles.standard

.. image:: img/20221011115048.png

Enfin, on se rend sur le dossier et on ouvre celui-ci depuis le fil d'ariane.
Il faut récupérer le **noderef** du dossier qui est dans l'URL.

.. image:: img/20221011115312.png

.. image:: img/20221011123332.png

Étape 2 - Référencer le dossier dans drupal
-------------------------------------------

Cette partie s'effectue en **SSH** et sur la base de données **d7**.

Premièrement, on recherche si le dossier est déjà référencé dans la BDD.

.. code-block:: sql

    SELECT * FROM field_data_field_folder_reference WHERE entity_id = 577194;

S'il y un résultat alors il faut mettre à jour la colonne **field_folder_reference_value**.

.. code-block:: sql

    UPDATE field_data_field_folder_reference SET field_data_field_folder_reference = "workspace://SpacesStore/fe4cfc71-bd66-45a8-af7d-28d65a5d9c19" WHERE entity_id = 577194;
    UPDATE field_revision_field_folder_reference SET field_data_field_folder_reference = "workspace://SpacesStore/fe4cfc71-bd66-45a8-af7d-28d65a5d9c19" WHERE entity_id = 577194;

Sinon, il faut insérer une nouvelle ligne.
Il faut récupérer le **revision id (vid)**.

.. code-block:: sql

    SELECT * from node_revision where nid = 577194;

.. image:: img/20221011124446.png

Et pour finir, insérer la ligne.
Ici :

- 577194 - node id
- 578972 - revision id
- workspace... - node ref

.. code-block:: sql

    INSERT INTO field_data_field_folder_reference VALUES ("node", "extranet", 0, 577194, 578972, "und", 0, "workspace://SpacesStore/fe4cfc71-bd66-45a8-af7d-28d65a5d9c19", NULL)
    INSERT INTO field_revision_field_folder_reference VALUES ("node", "extranet", 0, 577194, 578972, "und", 0, "workspace://SpacesStore/fe4cfc71-bd66-45a8-af7d-28d65a5d9c19", NULL);
