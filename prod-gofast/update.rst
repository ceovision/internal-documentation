********************************************
DOC - Update GoFAST
********************************************

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
  EPA          04/10/2022       V1.1
========= ==================== =======

Mise à jour de GoFAST
==========================

$URL = URL machine client (WEB)

La procédure d'update d'une GoFAST se passe en deux étapes :

- Ouverture du ticket entre nous et le client en question pour la demande de planification (date, heure etc)
- Mise à jour
    - vérification que personne n'utilise la plateforme en même temps 
    - suivi des logs de mise à jour MAIN 
    - suivi des logs de mise à jour COMM

En effet, avant de lancer quelconque mise à jour, il est important de s'assurer que personne n'est entrain d'utiliser activement la plateforme (coédition, visio etc)

Pour cela sur la MAIN vous pouvez regarder le traffic dans les logs de ce fichier :

::

    tail -f /var/log/drupal et /var/log/alfresco/alfresco.log 


Parce qu'il faut savoir qu'une mise à jour mets la plateforme en mode "maintenance" est n'est donc plus accessible durant une trentaine de minutes.

Une étape très importante également durant la mise à jour est le suivi des logs de mise à jour pour être sur que rien ne se bloque.

MAIN :

- Si avant 3.9.0 :

::

  tail -f /var/www/d7/sites/default/files/update_report.gflog  

- Si après 3.9.0

::

  tail -f /var/www/d7/sites/default/files/logs/update_report.gflog  


COMM :

::

    tail -f /opt/gofast-comm/update_report.txt


Interface mise à jour :
-------------------------

https://$URL/admin/config/gofast/update

Il vous suffis donc de cliquer sur le bouton de mise à jour et vérifier que le script se déroule bien dans les logs présentés précédemment.


Test Migration Bonita ( 4.0.0 -> 4.0.0_r2)  :
------------------------------

.. warning::
    Lors du passage de GoFAST 4.0.0 -> 4.0.0_r2 bien tester la migration Bonita avant de lancer la mise à jour.
    Si blocage lors de ce test de migration, supprimer les "process" indiqués.

::

    GF_VERSION=4.0.0_r2

    INSTALL_TYPE="enterprise"
    REPO_PATH="http://dev2.ceo-vision.com/sites/default/files/sources-3.x/${GF_VERSION}/"
    #Retrieve wget authentification
    enterprise_key=`grep -oP "(?<=\['enterprise-key'\] = \").+?(?=\";)" /var/www/d7/sites/default/settings.php`
    hostname=$(hostname)
    password=`echo -n "$hostname:$enterprise_key" | base64`
    password=`echo -n $password | sed -e "s/ //g"`
    credentials="--user $hostname --password $password"

    admin_pass=`grep -oP "(?<=\['admin_password'\] = ').+?(?=';)" /var/www/d7/sites/default/settings.php`
    root_db_pass=`grep -oP "(?<=db.password=).+" /var/lib/tomcats/alfresco/shared/classes/alfresco-global.properties`
    comm_domain=`grep -oP "(?<=\['gofast-comm_domain'\] = ').+?(?=';)" /var/www/d7/sites/default/settings.php`
    ldap_root_pass=`grep -oP "(?<=ldap.synchronization.java.naming.security.credentials=).+" /var/lib/tomcats/alfresco/shared/classes/alfresco-global.properties`
    TOMCAT_HOME=/usr/share/tomcat
    TOMCAT_BASE=/var/lib/tomcats
    hostname=$(hostname)
    ALF_HOST=`grep -oP "(?<=alfresco.host=).+" /var/lib/tomcats/alfresco/shared/classes/alfresco-global.properties`
    BASE_URL=https://${ALF_HOST}
    ldap_domain=`grep -oP "(?<=Manager,dc).+(?=,dc)" /var/lib/tomcats/alfresco/shared/classes/alfresco-global.properties | cut -c3-`

    ldap_domain_count=${#ldap_domain}
    ldap_tld_count=$(($ldap_domain_count+8))
    ldap_tld=`grep -oP "(?<=Manager,dc).+" /var/lib/tomcats/alfresco/shared/classes/alfresco-global.properties | cut -c${ldap_tld_count}-`



    cd /opt
    wget ${credentials} ${REPO_PATH}sources/bonita-migration-distrib-2.55.0.zip
    unzip bonita-migration-distrib-2.55.0.zip

    ls
    cd bonita-migration-distrib-2.55.0
    ls
    cd ..
    root_db_pass=`grep -oP "(?<=db.password=).+" /var/lib/tomcats/alfresco/shared/classes/alfresco-global.properties`
    sed -i -e "s/db.vendor=postgres/db.vendor=mysql/g" ./bonita-migration-distrib-2.55.0/Config.properties
    sed -i -e "s/db.url=jdbc:postgresql:\/\/localhost:5432\/bonita/db.url=jdbc:mysql:\/\/localhost:3306\/bonita?allowMultiQueries=true\&dontTrackOpenResources=true\&useUnicode=true\&useSSL=false\&characterEncoding=UTF-8/g" ./bonita-migration-distrib-2.55.0/Config.properties
    sed -i -e "s/db.driverClass=org.postgresql.Driver/db.driverClass=com.mysql.jdbc.Driver/g" ./bonita-migration-distrib-2.55.0/Config.properties
    sed -i -e "s/db.password=bpm/db.password=${root_db_pass}/g" ./bonita-migration-distrib-2.55.0/Config.properties
    cd ./bonita-migration-distrib-2.55.0/bin/
    export BONITA_MIGRATION_DISTRIB_OPTS="-Dtarget.version=7.13.0 -Dauto.accept=true"
    ls
    cd check-migration-dryrun
    ./check-migration-dryrun


::

    #[ERROR] Cannot create PoolableConnectionFactory (The server time zone value 'CEST' is unrecognized or represents more than one time zone. You must configure either the server or JDBC driver (via the serverTimezone configuration property) to use a more specifc time zone value if you want to utilize time zone support.) : 
    mysql -u root -p
    set GLOBAL time_zone = '+2:00';


Problèmes potentiels :
=========================

Erreur Deploiement ".war" :
------------------------------

::

    cd /var/lib/tomcats/alfresco/webapps/
    ls
    rm -Rf alfresco
    rm -Rf share
    systemctl restart tomcat@alfresco

(remettre les notfound.jsp après l'update si ceux-ci n'y sont plus)

Erreur Restart Alfresco (classe : impersonnation):
------------------------------------------------------

.. image:: img/restart_alfresco.png 

::

    cd /opt/update
    cd tomcat/amps
    \cp -Rf lib/ceo-vision.jar /var/lib/tomcats/alfresco/webapps/alfresco/WEB-INF/lib/
    \cp -Rf ../webapps/alfresco/WEB-INF/classes/com /var/lib/tomcats/alfresco/webapps/alfresco/WEB-INF/classes/
    chown -R tomcat:tomcat /var/lib/tomcats/alfresco/webapps/alfresco/
    restorecon -vR /var/lib/tomcats/alfresco/webapps/alfresco/
    cd /opt/update
    systemctl restart tomcat@alfresco

Erreur "TargetSchema" (uniquement sur nos plateformes ceo-vision.com) :
------------------------------------------------------------------------

.. image:: img/target_schema.png 

::

    vi /var/lib/tomcats/alfresco/webapps/alfresco/WEB-INF/classes/alfresco/module/alfresco-share-services/module-context.xml 

Recherchez "targetSchema" ( "/targetSchema" sous vi)

Et rajoutez un "0" à la value

Erreur Mauvaise version de Java (pour Alfresco et Bonita 4.0.0 & 4.0.0_r2) :
--------------------

::

    #force JAVA 11
    alternatives --set java /usr/lib/jvm/java-11-openjdk-11.*/bin/java

    #force JAVA 1.8
    alternatives --set java /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.*/jre/bin/java


Erreur Zabbix :
--------------------

::

    chmod -R 777 /etc/zabbix/zabbix_agentd.d/

Check fonctionnement des services
=====================================

Services à vérifier :
-----------------------

.. warning::
    bien tester chaque services

- Alfresco (gestion des fichiers/dossiers)
- Drupal (workflow)
- OnlyOffice (coédition et cadena)
- Jitsi (visio à plus de 3, et avec des partages d'écrans, caméras etc)
- Preview (prévisualisation document)
- Chat Element (connexion automatique, discussion)

.. note::
    Si vous rencontrez des problèmes avec les différents services ci-dessus, je vous laisse les corriger avec `la section correctifs de la documentation d'installation GoFAST <https://ceo-vision-internal-documentation.readthedocs.io/fr/latest/gofast-install/git.html#potentiels-problemes-et-comment-les-resoudres>`_



