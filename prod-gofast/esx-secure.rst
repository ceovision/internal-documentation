*******************************
PROC - Sécurisation ESXi
*******************************

Cette procédure décrit les configurations applicables pour la sécurisation d'un ESXi.


Ces différentes configurations sont à appliquer à chaque nouvelle instanciation d'un ESXi et peut également servir pour la maintenance

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
JLE        06/02/2023           V1.0
========= ==================== =======

Utilisation du SSH
======================

En usage normal, le service SSH doit systématiquement être désactivé. Il peut au besoin être activé le temps de la connexion.

Si le SSH est activé, sur l'interface graphique de l'ESXi on a cette indication : 

.. image:: img/esx-secure-ssh-enabled.png

Pour l'activer ou le désactiver, il faut aller dans le menu "Hôte" => "Actions" => Services

.. image:: img/esx-secure-ssh-actions.png

Désactivation du service SLP
==============================

.. info::
    Ce service peut également être désactivé en suivant la procédure de configuration du pare-feu

A l'origine de l'incident sécurité Nevada, le service SLP doit être désactivé sur nos ESXi.

Pour vérifier si le service est désactivé, utiliser les commandes : 

.. code-block::

    chkconfig --list | grep slpd
    /etc/init.d/slpd status

Voici le retour attendu : 

.. image:: img/esx-secure-slpd-off.png

Si ce n'est pas le cas, les commandes suivantes doivent être passés en SSH sur l'Hôte : 

.. code-block::

    /etc/init.d/slpd stop
    esxcli network firewall ruleset set -r CIMSLP -e 0
    chkconfig slpd off

Passage des correctifs
==============================

Les correctifs des ESXi doivent être passés régulièrement selon les implications sécurité.

TODO : Documenter la récupération des patchs, les derniers ayant été fournis par Kévin Gauriot

Pour cet exemple, je vais passer les derniers correctifs sur un ESXi en version 7 (Patchs I et J).

La première chose à faire est de charger ces correctifs dans le filesystem, pour ce faire il suffit de se connecter à l'interface d'administration de l'ESXi et de naviguer dans le datastore via "Explorateur de banque de données".

.. image:: img/esx-secure-datastore.png

Charger le correctif dans le datastore au format .zip : 

.. image:: img/esx-secure-datastore-load-patch.png

Puis en SSH, passer le correctif en utilisant la commande suivante : 

.. code-block::

    esxcli software vib update --depot=/<path_to_vib_ZIP>/<ZIP_file_name>.zip


Pour mon exemple, étant donné que j'ai chargé mon patch dans "datastore1" et que mon patch est nommé "", la commande sera : 

.. code-block::

    esxcli software vib update --depot=/vmfs/volumes/datastore1/VMware-ESXi-7.0U3i-20842708-depot.zip

Si la mise a jour s'est bien passée, le retour doit indiquer : "The update completed successfully"

.. image:: img/esx-secure-datastore-updated.png

Un redémarrage de l'ESXi est neccaire (dans la pluspart des cas) pour finaliser le processus de mise à jour

.. code-block::

    reboot

Configuration du Pare-feu
=============================================

Par défaut le pare-feu de VMWare est très permissif et écoute sur l'interface internet sur énormément de ports.

La majorité des services écoutant sur ses ports ne sont pas nécessaires au bon fonctionnement de l'ESXi. De plus, nous souhaitons filtrer et restreindre ces ports pour nos usages.

.. image:: img/esx-secure-default-firewall.png

Tous les services non grisés sont activés dans le firewall, dont une proportion importante de ports entrants non utilisés

.. warning::
    Via SSH, vérifier que le firewall est bien activé au niveau système en utilisant la commande 'esxcli network firewall get'

La gestion du pare-feu se fait dans l'interface WEB dans le menu "Mise en réseau" => "Règles du pare-feu" **OU** via SSH.

Désactivation de services
-----------------------------

Les services que nous souhaitons laisser opérationnels sont : 

- Client DHCP
- Client DNS
- DHCPv6
- Serveur SSH
- vSphere Web Client

Cas particulier d'un montage SAN pour Scaleway : 

- Client de logiciel iSCSI (Port 3260)

Tous les autres services doivent être **désactivés** dans le firewall (Clique droit => désactiver).

Filtrage IP
--------------

Les services de l'ESXi doivent être filtrés sur 3 IPs ou plages d'IPs : 

- L'IP de CEO-Vision
- Le réseau "Management network"
- Une IP de secours

.. warning::
    Il est probable que pour le service Client de logiciel iSCSI il faille également ajouter l'adresse IP du SAN, à vérifier sur une instance de test avant de modifier les pare-feu scaleway.

**IP de CEO-Vision** : 212.106.122.77 => Attention si cette IP venait à changer

**Réseau "Management network"** : Il s'agit du réseau de gestion de l'ESXi. La pluspart du temps il est dans l'URL d'accès à l'interface WEB, sinon il peut se trouver en allant voir les propriétés du vSwitch0

.. image:: img/esx-secure-management-network.png

Dans cet exemple, il s'agit du réseau 135.125.9.0/24 (Voir l'URL ou la ligne soulignée dans la capture d'écran).

**Une IP de secours** : Je propose de prendre l'IP de notre serveur de production GoFAST chez Celeste : 80.245.17.87/32

**Résumé** : 212.106.122.77/32,80.245.17.87/32,URL.0/32

**Pour mon exemple** : 212.106.122.77/32,80.245.17.87/32,135.125.9.0/32

Il suffit donc de copier cette chaine et de la coller dans les paramètres de chaque règles qu'on a laissé activé : 

.. image:: img/esx-secure-ip-config.png