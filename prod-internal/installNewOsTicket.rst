**************************************************
[WIP] PROC - osTicket sur Centos 7
**************************************************

Cette procédure décrit les étapes pour l'installation d'osTicket en Centos 7.

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
BCR        16/09/2022           V1.0
========= ==================== =======

Pré-requis 
============

- Un ESX où il est possible de créer une machine avec 4vCores, 8Go RAM, 100Go de stockage.
- Un OS Centos 7 à jour.
- Un serveur web apache, php (7.4), mySQL (5.5minimum)

Schéma
==============

.. image:: img/osTicketArchi.png

Installation 
==============

VM
----

La VM est hébergé sur ESXSN. Ses caractéristiques sont: 4vCPU, 8Go RAM, 100Go Disk.


Apache
-------

Installation apache.
::
    yum install httpd
    systemctl enable httpd
    install mod_ssl openssl
    # ensuite upload des certificats dans les répertoire /etc/pki/tls/...
    # modification de la conf ssl /etc/httpd/conf.d/ssl.conf
    # redirection port 80 dans /etc/httpd/conf.d/httpd.conf
    systemctl restart httpd

PHP 7.4
-------

Installation php 7.4.
::
    # get des repos necessaires
    yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    yum -y install https://rpms.remirepo.net/enterprise/remi-release-7.rpm
    # enable de php 7.4
    yum -y install yum-utils
    yum-config-manager --enable remi-php74
    # install php
    yum update
    yum install php php-cli php-fpm php-mysqlnd php-zip php-devel php-gd php-mcrypt php-mbstring php-curl php-xml php-pear php-bcmath php-json php-opcache php-xdebug php-memcache php-ldap php-imap
    # check version php
    php -v

MySQL
-----

docs: https://dev.mysql.com/doc/mysql-yum-repo-quick-guide/en/
::
    wget https://dev.mysql.com/get/mysql80-community-release-el7-7.noarch.rpm
    rpm -Uvh mysql80-community-release-el7-7.noarch.rpm
    sudo yum-config-manager --disable mysql80-community
    sudo yum-config-manager --enable mysql57-community
    yum update
    yum install mysql-server
    systemctl start mysqld
    systemctl enable mysqld
    systemctl status mysqld
    sudo grep 'temporary password' /var/log/mysqld.log
    mysql_secure_installation

MAIL
------------

Envoi de mail par osTicket.
::
    yum install sendmail
    yum install postfix
    # si erreur car il manque la librairie libmysqlclient.so.18
    yum install mariadb-libs-5.5.68-1.el7.x86_64

Sécurisation
------------

firewalld
::
    systemctl start firewalld
    systemctl enable firewalld
    firewall-cmd --get-default-zone
    firewall-cmd --set-default-zone=public
    firewall-cmd --zone=public --add-interface=<A REMPLIR>
    firewall-cmd --zone=public --add-service=https --permanent
    firewall-cmd --zone=public --add-service=http --permanent
    firewall-cmd --zone=public --add-service=ssh --permanent
    firewall-cmd --zone=public --add-service=smtp --permanent
    firewall-cmd --zone=public --add-icmp-block-inversion --permanent
    firewall-cmd --reload
    firewall-cmd --zone=public --list-all

Migration
=========


Copie des fichiers dans /var/www/osticket - ne devrait pas poser de problème

SQL
---

Copier de la base de données du serveur en prodution avec la commande suivante :
::
    mysqldump -u root -p --single-transaction --all-databases --events > mysql_dump.sql
    scp mysql_dump.sql root@helpdesk.ceo-vision.com:/root/
    mysql -u root -p < mysql_dump.sql

PHP
---

Copier le fichier de configuration (attention au mot de passe).
::
    scp /var/www/osticket/include/ost-config.php root@helpdesk.ceo-vision.com:/var/www/osticket/include

Backup
=======

Via veeam, ajouter le serveur et installer l'agent linux. 

Test à effectuer
=================

- Envoie d'un mail
- Création d'un ticket par mail
- Création d'un ticket par l'interface
- Redirection de l'ancien URL 
- Upload d'image
- Réponse à un ticket (ouverture/réponse/fermeture etc...)

Troubleshooting
================

Configuration de selinux qui parfois empêche l'accès.
