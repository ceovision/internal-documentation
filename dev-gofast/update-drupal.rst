*********************************************
DOC - Drupal : Mise à jour du coeur de Drupal
*********************************************

Pour mettre à jour Drupal, nous devons faire plus que simplement exécuter une commande étant donné que nous avons patché à la main certains fichiers et que nous devons donc nous assurer que ces modifications survivent à travers les différentes mises à jour de Drupal.

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
RBA       03/02/2023           V1.0
========= ==================== =======


Pré-requis :
============

.. warning::
    Cette documentation est conçue pour GoFAST 4.1.0 à 4.9.9.


Mise à jour de la base de code GoFAST :
=======================================

1. En ssh sur un serveur de développement (serveur MAIN), mettre à jour Drupal avec la commande suivante : ``drush @d7 pm-update drupal-X.XX -y``

.. note::
    Il est important d'expliciter la version de Drupal (p. ex. remplacer X.XX par 7.92 si la version cible est la v7.92), sans quoi le suivi strict des versions sera rendu impossible.

2. Télécharger la totalité du dossier Drupal ``/var/wwww/d7`` suite à la mise à jour.

3. Chercher la totalité des occurrences de "@patch ceo-vision" dans le dépôt git gofast.

.. warning::
    Il y a des variations. Parfois CEO est écrit en majuscule, parfois il n'y a pas de tiret. Le mieux est tout d'abord de vérifier les fichiers présents dans le dossier "core-patch" du dépôt gofast-install : ils ont tous au moins un patch CEO-Vision. Puis de chercher dans le dépôt gofast toutes les occurrences de ``patch.*ceo`` **MAIS AUSSI** de ``ceo.*patch`` (en regex) de manière case-insensitive pour être sûr de ne rien rater.

4. Réappliquer tous les patches ceo-vision.

5. Commit les changements dans le dépôt gofast.

Script d'installation :
=======================

1. Le script de mise à jour doit comporter la commande de mise à jour Drupal : ``drush @d7 pm-update drupal-X.XX -y``

2. Regarder les fichiers répertoriés dans le dossier "core_patch" : si l'un des fichiers a été modifié par la mise à jour de Drupal, il faudra écraser le fichier par sa version mise à jour dans le dépôt gofast.

3. Mettre à jour le numéro de version de Drupal dans le fichier "gofast_makefile.mk".

4. Boire le café de la récompense :-)