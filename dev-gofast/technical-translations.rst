**********************************************************************
DOC - Chaînes techniques : Utiliser des chaînes techniques dans GoFAST
**********************************************************************

Par défaut, Drupal 7 part du principe que la langue source est l'anglais. Ce paramètre est modifiable mais des milliers de chaînes traduites plus tard il est à présent trop tard pour rétropédaler.

Dans certaines situations, nous avons besoin de traduire depuis des chaînes techniques et non pas depuis l'anglais. En effet, si une personne autre qu'un développeur veut modifier des chaînes de GoFAST depuis un outil graphique tel que Weblate, par défaut la personne ne pourra modifier que la traduction française, mais pas la chaîne d'origine en anglais. En revanche, si la chaîne d'origine est "neutre" (chaîne technique), la personne pourra modifier la chaîne dans toutes les langues y compris l'anglais. Cela permet de décharger les développeurs des sollicitations liées aux changements de chaînes.

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
RBA       30/01/2023           V1.0
========= ==================== =======

Pré-requis :
============

.. warning::
    Cette documentation est conçue pour GoFAST 4.1.0 à 4.9.9.


Implémentation dans le code :
=============================

Mécanisme
---------
Une langue factice, le "GoFAST English" (gfen) a été ajoutée. Cette langue sert de langue source pour la traduction en anglais lorsque la chaîne source est une chaîne technique. Dans la mesure où Drupal considère toujours la chaîne source comme de l'anglais, lorsqu'une chaîne technique est utilisée, il faut appeler d'autres fonctions que celles proposées en natif par Drupal.

Appels et conventions
---------------------
Les fonctions à utiliser pour afficher la traduction d'une chaîne technique sont ``gft`` (en PHP) et ``Drupal.gft`` (en JS). Elles prennent les mêmes arguments que leurs équivalent natifs. Attention, ces fonctions exigent :

1. que la chaîne ait un contexte
2. que ce contexte soit formaté ainsi : ``gofast_technical:CONTEXT`` ; si le contexte n'est pas préfixé par ``gofast_technical:``, les commandes d'import et d'export ne feront pas passer la chaîne

Import et export
----------------
Des commandes drush ont été ajoutées pour importer et exporter des chaînes techniques.

Pour exporter les chaînes techniques actuellement présentes sur le serveur, il faut taper la commande drush :

``drush @d7 export-technical-translations LANGCODE CONTEXT``

par exemple :

``drush @d7 export-technical-translations gfen gofast_tour``

pour exporter les traductions anglaises de toutes les chaînes techniques ayant le contexte ``gofast_technical:gofast_tour``. Il n'y a volontairement pas de commande permettant d'exporter tous les contextes d'un coup étant donné que dans Weblate, les traductions sont silotées par contexte.

.. note::
    Le fichier sera exporté dans ``/var/www/d7/sites/all/modules/gofast/po/`` avec le nom ``LANGCODE_gofast_technical_CONTEXT.po``. Exemple : ``gfen_gofast_technical_gofast_tour.po``.

La commande d'import est symétrique de la commande d'export. Ainsi, la commande suivante :

``drush @d7 import-technical-translations gfen gofast_tour``

importera le contenu du fichier /var/www/d7/sites/all/modules/gofast/po/gfen_gofast_technical_gofast_tour.po.

.. warning::
    La commande d'import fait le ménage avant d'importer en supprimant toutes les chaînes précédentes associées au contexte et à la langue cibles. Assurez-vous que votre fichier .po soit complet avant de procéder à l'import.

Export dans Weblate :
=====================

Dépôt git
---------
Weblate a besoin d'un dépôt git lorsqu'il faut créer un nouveau composant de traductions. Le dépôt est publiquement accessible à l'adresse suivante :

``https://gitlab.com/ceovision/gofast-technical-weblate``

Les chaînes d'un contexte donné doivent être commit dans le dossier correspondant, p. ex. si le contexte est "gofast_technical:gofast_tour", les fichiers .po correspondants doivent être copiées dans le dossier "gofast_tour" sans modifier leur nom.


Accès à Weblate
---------------
Weblate est accessible à l'adresse suivante :

``http://translate.ceo-vision.com/``

Si vous n'avez pas de compte Weblate, vous pouvez demander à quelqu'un de l'équipe Support/Production de vous en créer un.

Le projet dédié aux chaînes techniques est "Gofast APP Technical Strings" :

.. image:: img/technical-translations-1.png

Créer un nouveau composant Weblate
----------------------------------
Weblate organise chaque projet en "composants". Dans le projet "GoFAST App Technical Strings", le découpage des composants doit correspondre aux contextes de traduction de Drupal. P. ex. un contexte "gofast_technical:gofast_tour" doit être associé à un composant "GoFAST Tour". Si le contexte de vos chaînes n'existe pas dans Weblate, vous devez créer un nouveau composant en cliquant sur "Ajouter un nouveau composant de traduction".

.. image:: img/technical-translations-2.png

Le contrôle de version doit être configuré de la manière suivante :

.. image:: img/technical-translations-3.png

On souhaite ensuite préciser nous-même quels .po doivent être importés à partir du dépôt :

.. image:: img/technical-translations-4.png

Le champ à modifier avant d'enregistrer est "Motif de fichier", qui doit prendre la valeur ``context/*.po``, p. ex. ``gofast_admin/*.po`` si le contexte est ``gofast_technical:gofast_admin``.

.. image:: img/technical-translations-5.png

Mettre à jour un composant existant
-----------------------------------

.. warning::
    Avant de mettre à jour les traductions, assurez-vous qu'elles soient bien à jour dans le dépôt git weblate-technical.

L'idée est de réimporter les chaînes à zéro en écrasant tout ce qui existait au préalable pour éviter de cumuler à travers le temps des chaînes obsolètes.

Allez dans la page de la langue cible, puis cliquez sur Gérer => Maintenance du dépôt :

.. image:: img/technical-translations-6.png

L'option qui nous intéresse dans les Outils du dépôt est "Réinitialiser" :

.. image:: img/technical-translations-7.png

That's all for today, folks!