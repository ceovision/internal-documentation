********************************************
DOC - Jira : Gestion des tâches
********************************************

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
  JLE          05/01/2023       V1.1
========= ==================== =======

Workflow de développement
==============================================

Cette documentation décrit le Workflow de développement JIRA de CEO-Vision pour GoFAST

.. image:: img/jira-workflow.png

Proposition
--------------

Cet état est l'état initial d'un Jira nouvellement crée. Le but de cet état est de receuillir sufisament de spécifications et d'être approuvé pour passer à la phase suivante.

.. WARNING::
   05.01.2023 : 'Proposition' est actuellement un état qui sert de Backlog depuis le début et n'est pas revu en comité régulier. En 2023 il est prévu de créer ce comité et de ne plus se servir de l'état 'Proposition' comme Backlog définitif.

aaa
````````````````````````````````````````````

Phase de développement
------------------------



Phase de production
---------------------

